package snake;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import snake.score.Score;
import snake.utils.Configurations;

import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JSeparator;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Window.Type;
import javax.swing.ImageIcon;
import java.awt.SystemColor;

public class MenuView extends JFrame {

	private JPanel contentPane;
	private MenuController _controller;
	private JLabel name1, name2, name3;
	private JLabel score1, score2, score3;

	/**
	 * Create the frame.
	 */
	public MenuView(MenuController controller) {
		this();
		_controller = controller;
	}
	
	public MenuView() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					defineComponents();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private void defineComponents() {
		setBackground(new Color(255, 255, 255));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 389, 239);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JComboBox cmbVelocity = new JComboBox();
		cmbVelocity.setBackground(Color.WHITE);
		cmbVelocity.setFont(new Font("Trebuchet MS", Font.BOLD, 11));
		cmbVelocity.setModel(new DefaultComboBoxModel(new String[] {"F\u00E1cil", "M\u00E9dio", "Dif\u00EDcil"}));
		cmbVelocity.setBounds(276, 11, 87, 20);
		contentPane.add(cmbVelocity);
		
		JButton btnIniciar = new JButton("Inicar");
		btnIniciar.setBackground(Color.WHITE);
		btnIniciar.setFont(new Font("Trebuchet MS", Font.BOLD, 11));
		
		MenuView menu = this;
		btnIniciar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int velocity = cmbVelocity.getSelectedIndex() == 0 ? 120
						: cmbVelocity.getSelectedIndex() == 1 ? 80 : 40;
				Configurations.THREAD_DELAY = velocity;
				new GardenController(menu);
				setVisible(false);
			}
		});
		btnIniciar.setBounds(276, 35, 87, 23);
		contentPane.add(btnIniciar);
		
		name1 = new JLabel("-");
		name1.setHorizontalAlignment(SwingConstants.RIGHT);
		name1.setBackground(Color.WHITE);
		name1.setForeground(Color.WHITE);
		name1.setFont(new Font("Trebuchet MS", Font.BOLD, 10));
		name1.setBounds(250, 97, 113, 14);
		contentPane.add(name1);
		
		name2 = new JLabel("-");
		name2.setHorizontalAlignment(SwingConstants.RIGHT);
		name2.setBackground(Color.WHITE);
		name2.setForeground(Color.WHITE);
		name2.setFont(new Font("Trebuchet MS", Font.BOLD, 10));
		name2.setBounds(250, 137, 113, 14);
		contentPane.add(name2);
		
		name3 = new JLabel("-");
		name3.setHorizontalAlignment(SwingConstants.RIGHT);
		name3.setBackground(Color.WHITE);
		name3.setForeground(Color.WHITE);
		name3.setFont(new Font("Trebuchet MS", Font.BOLD, 10));
		name3.setBounds(250, 175, 113, 14);
		contentPane.add(name3);
		
		score2 = new JLabel("0");
		score2.setHorizontalAlignment(SwingConstants.RIGHT);
		score2.setBackground(Color.WHITE);
		score2.setForeground(Color.WHITE);
		score2.setFont(new Font("Trebuchet MS", Font.BOLD, 13));
		score2.setBounds(338, 122, 25, 14);
		contentPane.add(score2);
		
		score1 = new JLabel("0");
		score1.setHorizontalAlignment(SwingConstants.RIGHT);
		score1.setBackground(Color.WHITE);
		score1.setForeground(Color.WHITE);
		score1.setFont(new Font("Trebuchet MS", Font.BOLD, 13));
		score1.setBounds(338, 82, 25, 14);
		contentPane.add(score1);
		
		score3 = new JLabel("0");
		score3.setHorizontalAlignment(SwingConstants.RIGHT);
		score3.setBackground(Color.WHITE);
		score3.setForeground(Color.WHITE);
		score3.setFont(new Font("Trebuchet MS", Font.BOLD, 13));
		score3.setBounds(338, 162, 25, 14);
		contentPane.add(score3);
		
		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setIcon(new ImageIcon(MenuView.class.getResource("/snake/resources/wpp.png")));
		lblNewLabel_1.setBounds(0, 0, 381, 205);
		contentPane.add(lblNewLabel_1);
		
		setVisible(true);
	}
	
	@Override
	public void setVisible(boolean visible) {
		if(!visible) {
			super.setVisible(false);
			return;
		}
		
		if(_controller == null) return;
		LinkedList<Score> scores = _controller.getTopScore();
		if(scores.size() >= 1) {
			name1.setText(scores.get(0).getName());
			score1.setText(scores.get(0).getScore().toString());
		}
		if(scores.size() >= 2) {
			name2.setText(scores.get(1).getName());
			score2.setText(scores.get(1).getScore().toString());
		}
		if(scores.size() >= 3) {
			name3.setText(scores.get(2).getName());
			score3.setText(scores.get(2).getScore().toString());
		}
		
		super.setVisible(true);
	}
}
