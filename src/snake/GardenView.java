package snake;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.border.EmptyBorder;

import snake.models.Apple;
import snake.models.Snake;
import snake.utils.Configurations;
import snake.utils.EnumDirection;
import javax.swing.border.LineBorder;

@SuppressWarnings("serial")
public class GardenView extends JFrame implements KeyListener {
	private JLabel _contentPane;
	private Snake _snake;
	private Apple _apple;
	private GardenController _controller;
	private Thread _thread;
	private boolean snakeColideToWall = false;

	public GardenView(GardenController controller) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					_controller = controller;
					defineComponents();
					defineProps();
				} catch (Exception e) {
					
					e.printStackTrace();
				}
			}
		});
	}
	
	public Snake getSnake() {
		return this._snake;
	}
	
	public Apple getApple() {
		return this._apple;
	}

	@Override
	public void keyPressed(KeyEvent e) {
		updateSnakeDirection(e);
	}

	@Override
	public void keyReleased(KeyEvent e) {
		updateSnakeDirection(e);
	}

	@Override
	public void keyTyped(KeyEvent e) {
		updateSnakeDirection(e);
	}
	
	private void defineComponents() {
		setVisible(true);
		addKeyListener(this);
		setBackground(new Color(146, 192, 3));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, Configurations.SCREEN_WIDTH, Configurations.SCREEN_HEIGHT);
		
		_contentPane = new JLabel();
		//_contentPane.setBorder(new LineBorder(new Color(139, 69, 19), 5));
		_contentPane.setBorder(new LineBorder(Color.BLACK, 5));
		//_contentPane.setBackground(new Color(60, 179, 113));
		_contentPane.setLayout(null);
		setContentPane(_contentPane);
	}
	
	private void defineProps() {
		_snake = new Snake(_contentPane);
		_apple = new Apple(_contentPane);

		_thread = new Thread() {
			public void run() {
				while(!snakeColideToWall) {
					try {
						Thread.sleep(Configurations.THREAD_DELAY);
						
						_snake.moveAndDraw();
						checkFood();
						checkWall();
						checkBody();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		};
		
		_thread.start();
	}
	
	private void checkFood() {
		boolean snakeEatApple = _snake.getHeadPosition().colideTo(_apple.getApplePosition());
		if(snakeEatApple) {
			_controller.snakeEatApple();
		}
	}
	
	private void checkWall() {
		snakeColideToWall = _snake.getHeadPosition().colideToScreen();
		if(snakeColideToWall) {
			_controller.snakeColide();
			dispose();
		}
	}
	
	private void checkBody() {
		boolean snakeHeadColideToBody = _snake.getHeadPosition().colideTo(_snake.getMembersPositions());
		if(snakeHeadColideToBody) {
			_thread.interrupt();
			_controller.snakeColide();
		}
	}
	
	private void updateSnakeDirection(KeyEvent e) {
		int keyCode = e.getKeyCode();
		switch (keyCode) {
			case KeyEvent.VK_UP: _controller.snakeChangeDirection(EnumDirection.up);
				break;
			case KeyEvent.VK_DOWN: _controller.snakeChangeDirection(EnumDirection.down);
				break;
			case KeyEvent.VK_LEFT: _controller.snakeChangeDirection(EnumDirection.left);
				break;
			case KeyEvent.VK_RIGHT: _controller.snakeChangeDirection(EnumDirection.right);
				break;
			default: break;
		}
	}
}
