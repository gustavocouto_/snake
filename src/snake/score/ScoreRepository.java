package snake.score;

import java.util.List;
import java.util.LinkedList;
import java.util.stream.Collectors;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;

@SuppressWarnings("unchecked")
public class ScoreRepository extends Repository {
	public ScoreRepository() {
		super();
	}
	
	public LinkedList<Score> getAll() {
		Criteria criteria = session.createCriteria(Score.class);
		criteria.addOrder(Order.desc("score"));
		List<Score> result = criteria.list();
		
		return (LinkedList<Score>)result.stream()
			.collect(Collectors.toCollection(LinkedList::new));
			
	}

	public void add(Score score) {
		session.save(score);
		saveChanges();
	}
	
	public void update(Score score) {
		session.save(score);
		saveChanges();
	}
	
	public Score get(String scoreName) {
		return session.get(Score.class, scoreName);
	}

	public void removeAll() {
		session.createQuery("delete from score");
	}
}