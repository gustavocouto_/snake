package snake.score;

import java.util.LinkedList;

public class ScoreService {
	private ScoreRepository _repository;
	
	public ScoreService() {
		_repository = new ScoreRepository();
	}

	public LinkedList<Score> getAll() {
		return _repository.getAll();
	}
	
	public Score get(String name) {
		return _repository.get(name);
	}

	public void addOrUpdate(Score score) {
		Score scoreBd = _repository.get(score.getName());
		if(scoreBd != null && (score.getScore() > scoreBd.getScore())) {
			scoreBd.updateScore(score.getScore());
			_repository.update(scoreBd);
		}
		else if(scoreBd == null) {
			_repository.add(score);
		}
	}

	public void removeAll() {
		_repository.removeAll();
	}
}