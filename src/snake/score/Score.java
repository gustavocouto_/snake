package snake.score;

import java.util.Date;

import javax.persistence.*;

public class Score {
	private String _name;
	private int _score;
	private Date _date;
	
	public Score() {
		
	}
	
	public Score(String name, int score) {
		_name = name;
		_score = score;
		_date = new Date();
	}
	
	protected Score(String name, int score, Date date) {
		this(name, score);
		_date = date;
	}

	public String getName() {
		return _name;
	}
	
	public void setName(String name) {
		_name = name;
	}

	public Integer getScore() {
		return _score;
	}
	
	public void setScore(int score) {
		_score = score;
	}

	public Date getDate() {
		return _date;
	}
	
	public void setDate(Date date) {
		_date = date;
	}
	
	public void updateScore(int newScore) {
		setScore(newScore);
		setDate(new Date());
	}
}