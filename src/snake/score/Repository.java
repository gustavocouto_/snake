package snake.score;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import snake.utils.Configurations;

public class Repository {
	private SessionFactory sessionFactory;
	protected Session session;
	
	public Repository() {
		sessionFactory = Configurations.CurrentSessionFactory;
		session = sessionFactory.openSession();
		session.beginTransaction();
	}
	
	protected void saveChanges() {
		session.getTransaction().commit();
		session.close();
	}
}
