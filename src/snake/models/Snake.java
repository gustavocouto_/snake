package snake.models;

import java.util.LinkedList;
import java.util.stream.Collectors;

import javax.swing.JLabel;

import snake.utils.EnumDirection;
import snake.utils.Position;

public class Snake {
	protected Head head;
	protected Member tail;
	private JLabel _panel;
	private LinkedList<Member> _members = new LinkedList<Member>();
	
	public Snake(JLabel panel) {
		tail = head = new Head();
		this._panel = panel;
		this._panel.add(head);
		
		_members.add(head);
	}
	
	public void addMember() {
		tail = new Body(this);
		_panel.add(tail);
		
		_members.add(tail);
	}

	public void moveAndDraw() {
		tail.moveAndDraw();
	}
	
	public void setDirection(EnumDirection direction) {
		if(head.currentDirection == EnumDirection.up && direction == EnumDirection.down) return;
		if(head.currentDirection == EnumDirection.down && direction == EnumDirection.up) return;
		if(head.currentDirection == EnumDirection.left && direction == EnumDirection.right) return;
		if(head.currentDirection == EnumDirection.right && direction == EnumDirection.left) return;
		
		head.currentDirection = direction;
	}
	
	public Position getHeadPosition() {
		return head.getPosition();
	}
	
	public LinkedList<Position> getMembersPositions() {
		Member neck = _members.size() >= 2 ? _members.get(1) : null;
		return _members.stream()
			.filter(m -> m != head && (neck == null || m != neck))
			.map(m -> m.getPosition())
			.collect(Collectors.toCollection(LinkedList::new));
	}
	
	public int getSize() {
		return _members.size();
	}
}
