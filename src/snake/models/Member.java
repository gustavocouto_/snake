package snake.models;

import java.awt.Color;
import javax.swing.JPanel;

import snake.utils.Configurations;
import snake.utils.Position;

@SuppressWarnings("serial")
public abstract class Member extends JPanel {
	int x, y;
	
	public Member(Color color) {
		setSize(Configurations.MEMBER_SIZE, Configurations.MEMBER_SIZE);
		setBackground(color);
	}
	
	public Member() {
		setSize(Configurations.MEMBER_SIZE, Configurations.MEMBER_SIZE);
		setBackground(new Color(106, 198, 89));
	}
	
	public Position getPosition() {
		return new Position(x, y);
	}
	
	public abstract void moveAndDraw();
	public abstract void goTo(Member member);
}