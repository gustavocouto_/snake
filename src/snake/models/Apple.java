package snake.models;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JPanel;

import snake.utils.Configurations;
import snake.utils.Position;

@SuppressWarnings("serial")
public class Apple extends JPanel {
	int x, y;
	
	private Apple() {
		setBackground(Color.red);
		setSize(Configurations.APPLE_SIZE, Configurations.APPLE_SIZE);
	}
	
	public Apple(JLabel panel) {
		this();
		panel.add(this);
		
		redraw();
	}
	
	public void redraw() {
		x = ((int)(Math.random() * Configurations.getAppleXLimit()));
		y = ((int)(Math.random() * Configurations.getAppleYLimit()));
		
		setLocation(x, y);		
	}
	
	public Position getApplePosition() {
		return new Position(x, y);
	}
}