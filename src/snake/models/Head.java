package snake.models;

import java.awt.Color;

import snake.utils.Configurations;
import snake.utils.EnumDirection;

@SuppressWarnings("serial")
public class Head extends Member {
	public EnumDirection lastDirection, currentDirection;
	
	public Head() {
		super(Color.black);
		//super(new Color(73, 130, 66));
		x = 200;
		y = 200;
		lastDirection = currentDirection = EnumDirection.up;
		setLocation(x, y);
	}
		
	public void moveAndDraw() {
		lastDirection = currentDirection;
		
		goTo(this);
		
		setLocation(x, y);
	}
	
	public void goTo(Member member) {
		if(((Head)member).currentDirection == EnumDirection.right)
			x += Configurations.MEMBER_SIZE;
		if(((Head)member).currentDirection == EnumDirection.left)
			x -= Configurations.MEMBER_SIZE;
		
		if(((Head)member).currentDirection == EnumDirection.up)
			y -= Configurations.MEMBER_SIZE;
		if(((Head)member).currentDirection == EnumDirection.down)
			y += Configurations.MEMBER_SIZE;
	}
}
