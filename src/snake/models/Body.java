package snake.models;

import java.awt.Color;

@SuppressWarnings("serial")
public class Body extends Member {
	private Member _next;

	public Body(Snake snake) {
		//super(new Color(106, 198, 89));
		super(Color.black);
		_next = snake.tail;
		goTo(_next);
	}
	
	public void moveAndDraw() {
		goTo(_next);

		setLocation(x, y);
		_next.moveAndDraw();
	}
	
	public void goTo(Member member) {
		x = member.x;
		y = member.y;
	}
}
