package snake.utils;

import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Configurations {
	public static final int MEMBER_SIZE = 10;
	public static final int APPLE_SIZE = 10;
	public static final int COLISION_ROUND = 10;
	public static int THREAD_DELAY = 80;
	public static final int SCREEN_WIDTH = 420;
	public static final int SCREEN_HEIGHT = 350;
	
	public static SessionFactory CurrentSessionFactory = new Configuration().configure().buildSessionFactory();;
	
	public static int getAppleXLimit() {
		return SCREEN_WIDTH - 100;
	}
	
	public static int getAppleYLimit() {
		return SCREEN_HEIGHT - 100;
	}
	
	public static Image getWall() {
		Image wall = null;
		String path = System.getProperty("user.dir") + "\\src\\snake\\resources\\wall.png";
		try {
			wall = ImageIO.read(new File(path));
			wall = wall.getScaledInstance(SCREEN_WIDTH, SCREEN_HEIGHT - 15, Image.SCALE_DEFAULT);
			
			return wall;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return wall; 
	}
}