package snake.utils;

import java.util.LinkedList;

public class Position {
	public int x;
	public int y;
	
	public Position(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public boolean colideTo(Position position) {
		boolean colideX = position.x > (x - Configurations.COLISION_ROUND)
				&& position.x < (x + Configurations.COLISION_ROUND);
		boolean colideY = position.y > (y - Configurations.COLISION_ROUND) 
				&& position.y < (y + Configurations.COLISION_ROUND);
		
		return colideX && colideY;
	}
	
	public boolean colideTo(LinkedList<Position> positions) {
		for(Position position: positions)
			if(colideTo(position))
				return true;
		
		return false;
	}
	
	public boolean colideToScreen() {
		boolean colideLeft = x <= 5;
		boolean colideRight = x >= Configurations.SCREEN_WIDTH - 30;
		boolean colideTop = y <= 5;
		boolean colideBottom = y >= Configurations.SCREEN_HEIGHT - 50;
		
		return colideLeft || colideRight || colideTop || colideBottom;
	}
}