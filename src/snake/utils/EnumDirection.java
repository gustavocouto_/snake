package snake.utils;

public enum EnumDirection {
	up,
	down,
	right,
	left
}
