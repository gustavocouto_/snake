package snake;

import java.util.LinkedList;

import snake.score.Score;
import snake.score.ScoreService;

public class MenuController {
	public MenuController() {
		new MenuView(this);
	}
	
	public LinkedList<Score> getTopScore() {
		return new ScoreService().getAll();
	}	
}
