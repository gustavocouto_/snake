package snake;

import javax.swing.JOptionPane;

import snake.score.Score;
import snake.score.ScoreService;
import snake.utils.EnumDirection;

public class GardenController {
	private GardenView _garden;
	private MenuView _menu;
	
	public GardenController(MenuView menu) {
		_garden = new GardenView(this);
		_menu = menu;
	}
	
	public void snakeEatApple() {
		_garden.getApple().redraw();
		_garden.getSnake().addMember();
	}
	
	public void snakeColide() {
		int size =  _garden.getSnake().getSize();
		String name = "";
		while(name == "" || name == null)
			name = JOptionPane.showInputDialog("Informe seu nome");
		Score score = new Score(name, size);
		
		new ScoreService().addOrUpdate(score);

		_menu.setVisible(true);
	}
	
	public void snakeChangeDirection(EnumDirection direction) {
		_garden.getSnake().setDirection(direction);
	}
}